# Архитектура компьютера. Лабораторная работа №3

# Forth. Транслятор и модель

- Васильев Максим Р33121
- `forth | stack | neum | hw | tick | struct | stream | mem | prob2`

## Язык программирования

``` ebnf
program ::= term

term ::= command
        | var_init
        | string_init
        | push_symbol
        | term term

command ::= "NOP" | "MOD" | "+" | "<" | "NEGATE" | "INVERT" | "DUP" | "OVER" | "ROT" | "SWAP" | "DROP"  
            | "IF" | "ELSE" | "ENDIF" 
            | "WHILE" | "BEGIN" | "REPEAT"
            | READ | READ# | WR | WR#

push_symbol = [-2^31; 2^31], ["a"; "z"]

string_init = (string_name): string_value

var_init ::= var_name: var_value 
                | &string_name

```

Код выполняется последовательно. Операции:

- `OVER` -- ... `a b` >> ... `a b a` 
- `DUP` -- ... `a` >> ... `a a`
- `<` -- ... `a b` >> ... `a<b`
- `MOD` -- ... `a b` >> ... `a%b`
- `NEGATE` -- ... `a` >> ... `-a`
- `INVERT` -- ... `0` >> ... `-1`
- `+` -- ... `a b` >> ... `a+b`
- `DROP` -- ... `a b` >> ... `a`
- `ROT` -- ... `a b c` >> ... `b c a`
- `SWAP` -- ... `a b` >> ... `b a`
- `{num}` -- ... `a b` >> ... `a b {num}`
- `{char}` -- ... `a b` >> ... `a b {char}`
- `IF` -- возьмёт со стека значение, и если оно True, то перейдёт далее. Если False, то прыгнет на ELSE
- `ELSE` -- сработает в случае, если при IF на стеке лежало False
- `ENDIF` -- закрывает блок кода, принадлежащий IF
- `BEGIN` -- является меткой, для возвращения назад, когда мы в цикле
- `WHILE` -- возьмёт со стека значение, и если оно True, то перейдёт далее. Если False, то прыгнет на команду, на +1 дальше, чем REPEAT
- `REPEAT` -- возвращает на BEGIN в случае если во время WHILE было True 
- `NOP` -- заглушка. не выполняет никаких операций
- `READ` (чтение с прямой адресацией)-- берёт со стека значение `x` и кладёт на стек значение memory[x]
- `READ#` (чтение с косвенной адресацией) -- берёт со стека значение `x`, кладёт на стек значение memory[memory[x]], и увеличивает значение memory[x] на +1
- `WR` (запись с прямой адресацией) -- берёт со стека значение `x`, и значение `y` и записывает в память memory[x] = y
- `WR#` (запись с косвенной адресацией) -- берёт со стека значение `x`, и значение `y` и записывает в память memory[memory[x]] = y, и увеличивает значение memory[x] на +1

## Организация памяти
Модель памяти процессора:

1. Память команд. Машинное слово -- не определено. Реализуется списком словарей, описывающих инструкции (одно слово -- одна ячейка).
2. Память данных. Машинное слово -- 32 бита, знаковое. Линейное адресное пространство. Реализуется списком чисел.
3. Стек. Заменяет регистры. Машинное слово -- 32 бита, знаковое. Линейное адресное пространство. Реализуется списком чисел.

Строки, объявленные пользователем распеделяются по памяти один символ на ячейку.

```text
     Instruction memory
+-----------------------------+
| 00  : jmp N                 |
| 01  : interrupt handler     |
|    ...                      | 
| n   : program start         |
|    ...                      |
+-----------------------------+

Data memory
+-----------------------------+
| 00  : input                 |
| 01  : output                |
| 03  : data                  |
|    ...                      |
| 127 : data                  |
| 127 : program               |
| 128 : program               |
|    ...                      |
+-----------------------------+
```

## Система команд

Особенности процессора:

- Машинное слово -- 32 бит, знаковое.
- `Память`:
    - адресуется через регистр `data_address`;
    - может быть записана:
        - с макушки стека;
        - с буффера ввода
    - может быть прочитана:
        - в буффер вывода;
- `Стек`:
    - адресуется через регистр `data_head`;
    - Верхушка стека (верхние 3 элемента):
        - может быть записана:
            - из буфера;
        - может быть прочитана:
            - на вход АЛУ
            - на вывод
        - используется как флаг `is_true` 1 - на верхушке `True`, иначе `False`
- Ввод-вывод -- stream.
- `program_counter` -- счётчик команд:
    - инкрементируется после каждой инструкции или перезаписывается инструкцией перехода.

### Набор инструкции

| Syntax         | Mnemonic        | Кол-во тактов | Comment                           |
|:---------------|:----------------|---------------|:----------------------------------|
| `OVER`         | OVER            | 1             | см. язык                          |
| `DUP`          | DUP             | 1             | см. язык                          |
| `<`            | LT              | 2             | см. язык                          |
| `MOD`          | MOD             | 2             | см. язык                          |
| `NEGATE`       | NEG             | 2             | см. язык                          |
| `INVERT`       | INV             | 2             | см. язык                          |
| `+`            | PLUS            | 2             | см. язык                          |
| `DROP`         | DROP            | 1             | см. язык                          |
| `ROT`          | ROT             | 1             | см. язык                          |
| `SWAP`         | SWAP            | 1             | см. язык                          |
| `{sym}`        | PUSH `{sym}`    | 1             | см. язык                          |
| `IF`           | JNT {ELSE + 1}  | 1             | см. язык                          |
| `ELSE`         | JMP {ENDIF}     | 1             | см. язык                          |
| `ENDIF`        | NOP             | 1             | см. язык                          |
| `BEGIN`        | BEGIN           | 1             | см. язык                          |
| `WHILE`        | JNT {REPEAT + 1}| 1             | см. язык                          |
| `REPEAT`       | JMP {BEGIN + 1} | 1             | см. язык                          |
| `NOP`          | NOP             | 1             | см. язык                          |
| `WR`           | WR_DIR          | 2             | см. язык                          |
| `WR#`          | WR_NDR          | 6             | см. язык                          |
| `READ`         | READ_DIR        | 2             | см. язык                          |
| `READ#`        | READ_NDR        | 7             | см. язык                          |


### Кодирование инструкций

- Машинный код сериализуется в список JSON.
- Один элемент списка, одна инструкция.
- Индекс списка -- адрес инструкции. Используется для команд перехода.

Пример:

```json
[
   {
        "opcode": "PUSH",
        "arg": "0",
        "term": [
            1,
            "PUSH",
            "0"
        ]
    }
]
```

где:

- `opcode` -- строка с кодом операции;
- `arg` -- аргумент (может отсутствовать);
- `term` -- информация о связанном месте в исходном коде (если есть).

Типы данные в модуле [isa](isa.py), где:

- `Opcode` -- перечисление кодов операций;
- `Term` -- структура для описания значимого фрагмента кода исходной программы.

## Транслятор

Интерфейс командной строки: `translator.py <input_file> <target_file> <data_section_file>"`

Реализовано в модуле: [translator](translator.py)

Этапы трансляции (функция `translate`):
1. Трансформирование текста в последовательность значимых термов.
    - Переменные:
        - Транслируются в соответствующие значения на этапе трансляции.
        - Задаётся либо числовым значением, либо указателем на начало строки (используя &string_name)
2. Проверка корректности программы (одинаковое количество IF, ELSE, ENDIF и BEGIN, WHILE, REPEAT).
3. Генерация машинного кода.

Правила генерации машинного кода:

- одно слово языка -- одна инструкция;
- для команд, однозначно соответствующих инструкциям -- прямое отображение;
- для циклов с соблюдением парности (многоточие -- произвольный код):

    | Номер команды/инструкции | Программа | Машинный код |
    |:-------------------------|:----------|:-------------|
    | n                        | `BEGIN`   | `BEGIN`      |
    | ...                      | ...       | ...          |
    | n+3                      | `WHILE`   | `JNT (k+1)`  |
    | ...                      | ...       | ...          |
    | k                        | `REPEAT`  |  `JMP (n+1)` |
    | k+1                      | ...       | ...          |
- для условных операторов (многоточие -- произвольный код):

    | Номер команды/инструкции | Программа | Машинный код |
    |:-------------------------|:----------|:-------------|
    | n                        | `IF`      | `JNT n+4`    |
    | ...                      | ...       | ...          |
    | n+3                      | `ELSE`    | `JMP (k+1)`  |
    | ...                      | ...       | ...          |
    | k                        | `ENDIF`   |  `NOP`       |
    | k+1                      | ...       | ...          |

## Модель процессора

Реализовано в модуле: [machine](machine.py).

![machine](img/machine.png)

Сигналы (обрабатываются за один такт, реализованы в виде методов класса Memory):

- `wr` -- записать в data memory
- `oe` -- прочитать из data memory 
- `latch_data_address` -- защёлкнуть значение в `data_address`

### DataPath

![data path](img/datapath.png)

Сигналы (обрабатываются за один такт, реализованы в виде методов класса):

- `push` -- записать на макушку стека значение из АЛУ, input или argument
- `oe_stack` -- прочитать из макушки стека и отправить в output
- `latch_data_head` -- защёлкнуть значение в `data_head`

Флаги:

- `is_true` -- лежит ли на стеке true.

### АЛУ

![alu](img/alu.png)

Сигналы (обрабатываются за один такт, реализованы в виде методов класса):

- `operation_sel` -- произвести операцию

### ControlUnit

![control unit](img/control_unit.png)

Реализован в классе `control_unit`.

- Hardwired (реализовано полностью на python).
- Моделирование на уровне тактов.
- Трансляция инструкции в последовательность (0-7 тактов) сигналов: `decode_and_execute`.

Сигнал:

- `latch_program_couter` -- сигнал для обновления счётчика команд в control_unit.

Особенности работы модели:

- Для журнала состояний процессора используется стандартный модуль logging.
- Количество инструкций для моделирования ограничено hardcoded константой.
- Остановка моделирования осуществляется при помощи исключений:
    - `StopIteration` -- если выполнена инструкция `halt`.
- Управление симуляцией реализовано в функции `simulate`.

## Апробация

В качестве тестов использовано 5 программ:

1. [hello world](examples/hello_world_code).
2. [cat](examples/cat_code) -- программа `cat`, повторяем ввод на выводе.
3. [prob2](examples/prob2_code) -- программа, считающая сумму четных элементов последовательности Фибонначи.
4. [if](examples/if_code) -- программа, проверяющая работу if.
5. [while](examples/while_code) -- программа, проверяющая работу while.

Голден-тесты: [golden](golden)


CI:

``` yaml
lab3-example:
  stage: test
  image:
    name: python-tools
    entrypoint: [ "" ]
  before_script:
    - pip install pytest-golden
  script:
    - python3-coverage run -m pytest --verbose
    - find . -type f -name "*.py" | xargs -t python3-coverage report
    - find . -type f -name "*.py" | xargs -t pep8 --ignore=E501
    - find . -type f -name "*.py" | xargs -t pylint
```

где:

- `pytest` -- утилита для запуска тестов.
- `python3-coverage` -- формирование отчёта об уровне покрытия исходного кода.
- `pep8` -- утилита для проверки форматирования кода. `E501` (длина строк) отключено, но не следует этим злоупотреблять.
- `pylint` -- утилита для проверки качества кода. Некоторые правила отключены в отдельных модулях с целью упрощения кода.
- Docker image `python-tools` включает в себя все перечисленные утилиты. Его конфигурация: [Dockerfile](./Dockerfile).

Пример использования и журнал работы процессора на примере `Hello_world`:

``` console
> cat examples/hello_world_code
(word): Hello world!
word_start: &word
word_ind: 127

word_start
word_ind
WR

BEGIN
    word_ind
    READ#
    0
    OVER
    <
    WHILE
    #out
    WR
REPEA
> ./translator.py examples/hello_world_code machine_code examples/data_section
source LoC: 18 code instr: 14
> cat machine_code 
[
    {
        "opcode": "PUSH",
        "arg": 2,
        "term": [
            5,
            "PUSH",
            2
        ]
    },
    {
        "opcode": "PUSH",
        "arg": "127",
        "term": [
            6,
            "PUSH",
            "127"
        ]
    },
    {
        "opcode": "WRITE_DIR",
        "term": [
            7,
            "WR",
            null
        ]
    },
    {
        "opcode": "BEGIN",
        "term": [
            9,
            "BEGIN",
            null
        ]
    },
    {
        "opcode": "PUSH",
        "arg": "127",
        "term": [
            10,
            "PUSH",
            "127"
        ]
    },
    {
        "opcode": "READ_NDR",
        "term": [
            11,
            "READ#",
            null
        ]
    },
    {
        "opcode": "PUSH",
        "arg": "0",
        "term": [
            12,
            "PUSH",
            "0"
        ]
    },
    {
        "opcode": "OVER",
        "term": [
            13,
            "OVER",
            null
        ]
    },
    {
        "opcode": "LT",
        "term": [
            14,
            "<",
            null
        ]
    },
    {
        "opcode": "JNT",
        "arg": 13,
        "term": [
            15,
            "WHILE",
            null
        ]
    },
    {
        "opcode": "PUSH",
        "arg": 1,
        "term": [
            16,
            "PUSH",
            1
        ]
    },
    {
        "opcode": "WRITE_DIR",
        "term": [
            17,
            "WR",
            null
        ]
    },
    {
        "opcode": "JMP",
        "arg": 4,
        "term": [
            18,
            "REPEAT",
            null
        ]
    },
    {
        "opcode": "HALT",
        "term": [
            14,
            "HALT",
            null
        ]
    }
]
> ./machine.py machine_code.out examples/data_section
DEBUG:root:{TICK: 1, PC: 1, HEAD: 1, TOS: 0, 0, 2}  Opcode.PUSH 127 ('127' @ 6:PUSH)
DEBUG:root:{TICK: 2, PC: 2, HEAD: 2, TOS: 0, 2, 127}  Opcode.WR_DIR  ('None' @ 7:WR)
DEBUG:root:{TICK: 3, PC: 2, HEAD: 2, TOS: 0, 2, 127}  Opcode.WR_DIR  ('None' @ 7:WR)
DEBUG:root:{TICK: 4, PC: 3, HEAD: 0, TOS: 0, 0, 0}  Opcode.BEGIN  ('None' @ 9:BEGIN)
DEBUG:root:{TICK: 5, PC: 4, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 127 ('127' @ 10:PUSH)
DEBUG:root:{TICK: 6, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 7, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 8, PC: 5, HEAD: 2, TOS: 0, 127, 2}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 9, PC: 5, HEAD: 2, TOS: 0, 127, 2}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 10, PC: 5, HEAD: 3, TOS: 127, 2, 72}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 11, PC: 5, HEAD: 3, TOS: 72, 2, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 12, PC: 5, HEAD: 3, TOS: 72, 2, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 13, PC: 6, HEAD: 1, TOS: 0, 0, 72}  Opcode.PUSH 0 ('0' @ 12:PUSH)
DEBUG:root:{TICK: 14, PC: 7, HEAD: 2, TOS: 0, 72, 0}  Opcode.OVER  ('None' @ 13:OVER)
DEBUG:root:{TICK: 15, PC: 8, HEAD: 3, TOS: 72, 0, 72}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 16, PC: 8, HEAD: 1, TOS: 0, 0, 72}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 17, PC: 9, HEAD: 2, TOS: 0, 72, -1}  Opcode.JNT 13 ('None' @ 15:WHILE)
DEBUG:root:{TICK: 18, PC: 10, HEAD: 1, TOS: 0, 0, 72}  Opcode.PUSH 1 ('1' @ 16:PUSH)
DEBUG:root:{TICK: 19, PC: 11, HEAD: 2, TOS: 0, 72, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:{TICK: 20, PC: 11, HEAD: 2, TOS: 0, 72, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:output:  << H
DEBUG:root:{TICK: 21, PC: 12, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 4 ('None' @ 18:REPEAT)
DEBUG:root:{TICK: 22, PC: 4, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 127 ('127' @ 10:PUSH)
DEBUG:root:{TICK: 23, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 24, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 25, PC: 5, HEAD: 2, TOS: 0, 127, 3}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 26, PC: 5, HEAD: 2, TOS: 0, 127, 3}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 27, PC: 5, HEAD: 3, TOS: 127, 3, 101}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 28, PC: 5, HEAD: 3, TOS: 101, 3, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 29, PC: 5, HEAD: 3, TOS: 101, 3, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 30, PC: 6, HEAD: 1, TOS: 0, 0, 101}  Opcode.PUSH 0 ('0' @ 12:PUSH)
DEBUG:root:{TICK: 31, PC: 7, HEAD: 2, TOS: 0, 101, 0}  Opcode.OVER  ('None' @ 13:OVER)
DEBUG:root:{TICK: 32, PC: 8, HEAD: 3, TOS: 101, 0, 101}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 33, PC: 8, HEAD: 1, TOS: 0, 0, 101}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 34, PC: 9, HEAD: 2, TOS: 0, 101, -1}  Opcode.JNT 13 ('None' @ 15:WHILE)
DEBUG:root:{TICK: 35, PC: 10, HEAD: 1, TOS: 0, 0, 101}  Opcode.PUSH 1 ('1' @ 16:PUSH)
DEBUG:root:{TICK: 36, PC: 11, HEAD: 2, TOS: 0, 101, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:{TICK: 37, PC: 11, HEAD: 2, TOS: 0, 101, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:output: H << e
DEBUG:root:{TICK: 38, PC: 12, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 4 ('None' @ 18:REPEAT)
DEBUG:root:{TICK: 39, PC: 4, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 127 ('127' @ 10:PUSH)
DEBUG:root:{TICK: 40, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 41, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 42, PC: 5, HEAD: 2, TOS: 0, 127, 4}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 43, PC: 5, HEAD: 2, TOS: 0, 127, 4}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 44, PC: 5, HEAD: 3, TOS: 127, 4, 108}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 45, PC: 5, HEAD: 3, TOS: 108, 4, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 46, PC: 5, HEAD: 3, TOS: 108, 4, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 47, PC: 6, HEAD: 1, TOS: 0, 0, 108}  Opcode.PUSH 0 ('0' @ 12:PUSH)
DEBUG:root:{TICK: 48, PC: 7, HEAD: 2, TOS: 0, 108, 0}  Opcode.OVER  ('None' @ 13:OVER)
DEBUG:root:{TICK: 49, PC: 8, HEAD: 3, TOS: 108, 0, 108}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 50, PC: 8, HEAD: 1, TOS: 0, 0, 108}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 51, PC: 9, HEAD: 2, TOS: 0, 108, -1}  Opcode.JNT 13 ('None' @ 15:WHILE)
DEBUG:root:{TICK: 52, PC: 10, HEAD: 1, TOS: 0, 0, 108}  Opcode.PUSH 1 ('1' @ 16:PUSH)
DEBUG:root:{TICK: 53, PC: 11, HEAD: 2, TOS: 0, 108, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:{TICK: 54, PC: 11, HEAD: 2, TOS: 0, 108, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:output: H,e << l
DEBUG:root:{TICK: 55, PC: 12, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 4 ('None' @ 18:REPEAT)
DEBUG:root:{TICK: 56, PC: 4, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 127 ('127' @ 10:PUSH)
DEBUG:root:{TICK: 57, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 58, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 59, PC: 5, HEAD: 2, TOS: 0, 127, 5}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 60, PC: 5, HEAD: 2, TOS: 0, 127, 5}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 61, PC: 5, HEAD: 3, TOS: 127, 5, 108}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 62, PC: 5, HEAD: 3, TOS: 108, 5, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 63, PC: 5, HEAD: 3, TOS: 108, 5, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 64, PC: 6, HEAD: 1, TOS: 0, 0, 108}  Opcode.PUSH 0 ('0' @ 12:PUSH)
DEBUG:root:{TICK: 65, PC: 7, HEAD: 2, TOS: 0, 108, 0}  Opcode.OVER  ('None' @ 13:OVER)
DEBUG:root:{TICK: 66, PC: 8, HEAD: 3, TOS: 108, 0, 108}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 67, PC: 8, HEAD: 1, TOS: 0, 0, 108}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 68, PC: 9, HEAD: 2, TOS: 0, 108, -1}  Opcode.JNT 13 ('None' @ 15:WHILE)
DEBUG:root:{TICK: 69, PC: 10, HEAD: 1, TOS: 0, 0, 108}  Opcode.PUSH 1 ('1' @ 16:PUSH)
DEBUG:root:{TICK: 70, PC: 11, HEAD: 2, TOS: 0, 108, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:{TICK: 71, PC: 11, HEAD: 2, TOS: 0, 108, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:output: H,e,l << l
DEBUG:root:{TICK: 72, PC: 12, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 4 ('None' @ 18:REPEAT)
DEBUG:root:{TICK: 73, PC: 4, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 127 ('127' @ 10:PUSH)
DEBUG:root:{TICK: 74, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 75, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 76, PC: 5, HEAD: 2, TOS: 0, 127, 6}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 77, PC: 5, HEAD: 2, TOS: 0, 127, 6}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 78, PC: 5, HEAD: 3, TOS: 127, 6, 111}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 79, PC: 5, HEAD: 3, TOS: 111, 6, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 80, PC: 5, HEAD: 3, TOS: 111, 6, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 81, PC: 6, HEAD: 1, TOS: 0, 0, 111}  Opcode.PUSH 0 ('0' @ 12:PUSH)
DEBUG:root:{TICK: 82, PC: 7, HEAD: 2, TOS: 0, 111, 0}  Opcode.OVER  ('None' @ 13:OVER)
DEBUG:root:{TICK: 83, PC: 8, HEAD: 3, TOS: 111, 0, 111}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 84, PC: 8, HEAD: 1, TOS: 0, 0, 111}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 85, PC: 9, HEAD: 2, TOS: 0, 111, -1}  Opcode.JNT 13 ('None' @ 15:WHILE)
DEBUG:root:{TICK: 86, PC: 10, HEAD: 1, TOS: 0, 0, 111}  Opcode.PUSH 1 ('1' @ 16:PUSH)
DEBUG:root:{TICK: 87, PC: 11, HEAD: 2, TOS: 0, 111, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:{TICK: 88, PC: 11, HEAD: 2, TOS: 0, 111, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:output: H,e,l,l << o
DEBUG:root:{TICK: 89, PC: 12, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 4 ('None' @ 18:REPEAT)
DEBUG:root:{TICK: 90, PC: 4, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 127 ('127' @ 10:PUSH)
DEBUG:root:{TICK: 91, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 92, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 93, PC: 5, HEAD: 2, TOS: 0, 127, 7}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 94, PC: 5, HEAD: 2, TOS: 0, 127, 7}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 95, PC: 5, HEAD: 3, TOS: 127, 7, 32}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 96, PC: 5, HEAD: 3, TOS: 32, 7, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 97, PC: 5, HEAD: 3, TOS: 32, 7, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 98, PC: 6, HEAD: 1, TOS: 0, 0, 32}  Opcode.PUSH 0 ('0' @ 12:PUSH)
DEBUG:root:{TICK: 99, PC: 7, HEAD: 2, TOS: 0, 32, 0}  Opcode.OVER  ('None' @ 13:OVER)
DEBUG:root:{TICK: 100, PC: 8, HEAD: 3, TOS: 32, 0, 32}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 101, PC: 8, HEAD: 1, TOS: 0, 0, 32}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 102, PC: 9, HEAD: 2, TOS: 0, 32, -1}  Opcode.JNT 13 ('None' @ 15:WHILE)
DEBUG:root:{TICK: 103, PC: 10, HEAD: 1, TOS: 0, 0, 32}  Opcode.PUSH 1 ('1' @ 16:PUSH)
DEBUG:root:{TICK: 104, PC: 11, HEAD: 2, TOS: 0, 32, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:{TICK: 105, PC: 11, HEAD: 2, TOS: 0, 32, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:output: H,e,l,l,o <<
DEBUG:root:{TICK: 106, PC: 12, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 4 ('None' @ 18:REPEAT)
DEBUG:root:{TICK: 107, PC: 4, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 127 ('127' @ 10:PUSH)
DEBUG:root:{TICK: 108, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 109, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 110, PC: 5, HEAD: 2, TOS: 0, 127, 8}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 111, PC: 5, HEAD: 2, TOS: 0, 127, 8}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 112, PC: 5, HEAD: 3, TOS: 127, 8, 119}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 113, PC: 5, HEAD: 3, TOS: 119, 8, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 114, PC: 5, HEAD: 3, TOS: 119, 8, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 115, PC: 6, HEAD: 1, TOS: 0, 0, 119}  Opcode.PUSH 0 ('0' @ 12:PUSH)
DEBUG:root:{TICK: 116, PC: 7, HEAD: 2, TOS: 0, 119, 0}  Opcode.OVER  ('None' @ 13:OVER)
DEBUG:root:{TICK: 117, PC: 8, HEAD: 3, TOS: 119, 0, 119}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 118, PC: 8, HEAD: 1, TOS: 0, 0, 119}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 119, PC: 9, HEAD: 2, TOS: 0, 119, -1}  Opcode.JNT 13 ('None' @ 15:WHILE)
DEBUG:root:{TICK: 120, PC: 10, HEAD: 1, TOS: 0, 0, 119}  Opcode.PUSH 1 ('1' @ 16:PUSH)
DEBUG:root:{TICK: 121, PC: 11, HEAD: 2, TOS: 0, 119, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:{TICK: 122, PC: 11, HEAD: 2, TOS: 0, 119, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:output: H,e,l,l,o,  << w
DEBUG:root:{TICK: 123, PC: 12, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 4 ('None' @ 18:REPEAT)
DEBUG:root:{TICK: 124, PC: 4, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 127 ('127' @ 10:PUSH)
DEBUG:root:{TICK: 125, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 126, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 127, PC: 5, HEAD: 2, TOS: 0, 127, 9}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 128, PC: 5, HEAD: 2, TOS: 0, 127, 9}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 129, PC: 5, HEAD: 3, TOS: 127, 9, 111}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 130, PC: 5, HEAD: 3, TOS: 111, 9, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 131, PC: 5, HEAD: 3, TOS: 111, 9, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 132, PC: 6, HEAD: 1, TOS: 0, 0, 111}  Opcode.PUSH 0 ('0' @ 12:PUSH)
DEBUG:root:{TICK: 133, PC: 7, HEAD: 2, TOS: 0, 111, 0}  Opcode.OVER  ('None' @ 13:OVER)
DEBUG:root:{TICK: 134, PC: 8, HEAD: 3, TOS: 111, 0, 111}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 135, PC: 8, HEAD: 1, TOS: 0, 0, 111}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 136, PC: 9, HEAD: 2, TOS: 0, 111, -1}  Opcode.JNT 13 ('None' @ 15:WHILE)
DEBUG:root:{TICK: 137, PC: 10, HEAD: 1, TOS: 0, 0, 111}  Opcode.PUSH 1 ('1' @ 16:PUSH)
DEBUG:root:{TICK: 138, PC: 11, HEAD: 2, TOS: 0, 111, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:{TICK: 139, PC: 11, HEAD: 2, TOS: 0, 111, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:output: H,e,l,l,o, ,w << o
DEBUG:root:{TICK: 140, PC: 12, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 4 ('None' @ 18:REPEAT)
DEBUG:root:{TICK: 141, PC: 4, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 127 ('127' @ 10:PUSH)
DEBUG:root:{TICK: 142, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 143, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 144, PC: 5, HEAD: 2, TOS: 0, 127, 10}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 145, PC: 5, HEAD: 2, TOS: 0, 127, 10}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 146, PC: 5, HEAD: 3, TOS: 127, 10, 114}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 147, PC: 5, HEAD: 3, TOS: 114, 10, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 148, PC: 5, HEAD: 3, TOS: 114, 10, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 149, PC: 6, HEAD: 1, TOS: 0, 0, 114}  Opcode.PUSH 0 ('0' @ 12:PUSH)
DEBUG:root:{TICK: 150, PC: 7, HEAD: 2, TOS: 0, 114, 0}  Opcode.OVER  ('None' @ 13:OVER)
DEBUG:root:{TICK: 151, PC: 8, HEAD: 3, TOS: 114, 0, 114}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 152, PC: 8, HEAD: 1, TOS: 0, 0, 114}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 153, PC: 9, HEAD: 2, TOS: 0, 114, -1}  Opcode.JNT 13 ('None' @ 15:WHILE)
DEBUG:root:{TICK: 154, PC: 10, HEAD: 1, TOS: 0, 0, 114}  Opcode.PUSH 1 ('1' @ 16:PUSH)
DEBUG:root:{TICK: 155, PC: 11, HEAD: 2, TOS: 0, 114, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:{TICK: 156, PC: 11, HEAD: 2, TOS: 0, 114, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:output: H,e,l,l,o, ,w,o << r
DEBUG:root:{TICK: 157, PC: 12, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 4 ('None' @ 18:REPEAT)
DEBUG:root:{TICK: 158, PC: 4, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 127 ('127' @ 10:PUSH)
DEBUG:root:{TICK: 159, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 160, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 161, PC: 5, HEAD: 2, TOS: 0, 127, 11}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 162, PC: 5, HEAD: 2, TOS: 0, 127, 11}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 163, PC: 5, HEAD: 3, TOS: 127, 11, 108}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 164, PC: 5, HEAD: 3, TOS: 108, 11, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 165, PC: 5, HEAD: 3, TOS: 108, 11, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 166, PC: 6, HEAD: 1, TOS: 0, 0, 108}  Opcode.PUSH 0 ('0' @ 12:PUSH)
DEBUG:root:{TICK: 167, PC: 7, HEAD: 2, TOS: 0, 108, 0}  Opcode.OVER  ('None' @ 13:OVER)
DEBUG:root:{TICK: 168, PC: 8, HEAD: 3, TOS: 108, 0, 108}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 169, PC: 8, HEAD: 1, TOS: 0, 0, 108}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 170, PC: 9, HEAD: 2, TOS: 0, 108, -1}  Opcode.JNT 13 ('None' @ 15:WHILE)
DEBUG:root:{TICK: 171, PC: 10, HEAD: 1, TOS: 0, 0, 108}  Opcode.PUSH 1 ('1' @ 16:PUSH)
DEBUG:root:{TICK: 172, PC: 11, HEAD: 2, TOS: 0, 108, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:{TICK: 173, PC: 11, HEAD: 2, TOS: 0, 108, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:output: H,e,l,l,o, ,w,o,r << l
DEBUG:root:{TICK: 174, PC: 12, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 4 ('None' @ 18:REPEAT)
DEBUG:root:{TICK: 175, PC: 4, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 127 ('127' @ 10:PUSH)
DEBUG:root:{TICK: 176, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 177, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 178, PC: 5, HEAD: 2, TOS: 0, 127, 12}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 179, PC: 5, HEAD: 2, TOS: 0, 127, 12}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 180, PC: 5, HEAD: 3, TOS: 127, 12, 100}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 181, PC: 5, HEAD: 3, TOS: 100, 12, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 182, PC: 5, HEAD: 3, TOS: 100, 12, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 183, PC: 6, HEAD: 1, TOS: 0, 0, 100}  Opcode.PUSH 0 ('0' @ 12:PUSH)
DEBUG:root:{TICK: 184, PC: 7, HEAD: 2, TOS: 0, 100, 0}  Opcode.OVER  ('None' @ 13:OVER)
DEBUG:root:{TICK: 185, PC: 8, HEAD: 3, TOS: 100, 0, 100}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 186, PC: 8, HEAD: 1, TOS: 0, 0, 100}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 187, PC: 9, HEAD: 2, TOS: 0, 100, -1}  Opcode.JNT 13 ('None' @ 15:WHILE)
DEBUG:root:{TICK: 188, PC: 10, HEAD: 1, TOS: 0, 0, 100}  Opcode.PUSH 1 ('1' @ 16:PUSH)
DEBUG:root:{TICK: 189, PC: 11, HEAD: 2, TOS: 0, 100, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:{TICK: 190, PC: 11, HEAD: 2, TOS: 0, 100, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:output: H,e,l,l,o, ,w,o,r,l << d
DEBUG:root:{TICK: 191, PC: 12, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 4 ('None' @ 18:REPEAT)
DEBUG:root:{TICK: 192, PC: 4, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 127 ('127' @ 10:PUSH)
DEBUG:root:{TICK: 193, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 194, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 195, PC: 5, HEAD: 2, TOS: 0, 127, 13}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 196, PC: 5, HEAD: 2, TOS: 0, 127, 13}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 197, PC: 5, HEAD: 3, TOS: 127, 13, 33}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 198, PC: 5, HEAD: 3, TOS: 33, 13, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 199, PC: 5, HEAD: 3, TOS: 33, 13, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 200, PC: 6, HEAD: 1, TOS: 0, 0, 33}  Opcode.PUSH 0 ('0' @ 12:PUSH)
DEBUG:root:{TICK: 201, PC: 7, HEAD: 2, TOS: 0, 33, 0}  Opcode.OVER  ('None' @ 13:OVER)
DEBUG:root:{TICK: 202, PC: 8, HEAD: 3, TOS: 33, 0, 33}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 203, PC: 8, HEAD: 1, TOS: 0, 0, 33}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 204, PC: 9, HEAD: 2, TOS: 0, 33, -1}  Opcode.JNT 13 ('None' @ 15:WHILE)
DEBUG:root:{TICK: 205, PC: 10, HEAD: 1, TOS: 0, 0, 33}  Opcode.PUSH 1 ('1' @ 16:PUSH)
DEBUG:root:{TICK: 206, PC: 11, HEAD: 2, TOS: 0, 33, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:{TICK: 207, PC: 11, HEAD: 2, TOS: 0, 33, 1}  Opcode.WR_DIR  ('None' @ 17:WR)
DEBUG:root:output: H,e,l,l,o, ,w,o,r,l,d << !
DEBUG:root:{TICK: 208, PC: 12, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 4 ('None' @ 18:REPEAT)
DEBUG:root:{TICK: 209, PC: 4, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 127 ('127' @ 10:PUSH)
DEBUG:root:{TICK: 210, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 211, PC: 5, HEAD: 1, TOS: 0, 0, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 212, PC: 5, HEAD: 2, TOS: 0, 127, 14}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 213, PC: 5, HEAD: 2, TOS: 0, 127, 14}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 214, PC: 5, HEAD: 3, TOS: 127, 14, 0}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 215, PC: 5, HEAD: 3, TOS: 0, 14, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 216, PC: 5, HEAD: 3, TOS: 0, 14, 127}  Opcode.READ_NDR  ('None' @ 11:READ#)
DEBUG:root:{TICK: 217, PC: 6, HEAD: 1, TOS: 0, 0, 0}  Opcode.PUSH 0 ('0' @ 12:PUSH)
DEBUG:root:{TICK: 218, PC: 7, HEAD: 2, TOS: 0, 0, 0}  Opcode.OVER  ('None' @ 13:OVER)
DEBUG:root:{TICK: 219, PC: 8, HEAD: 3, TOS: 0, 0, 0}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 220, PC: 8, HEAD: 1, TOS: 0, 0, 0}  Opcode.LT  ('None' @ 14:<)
DEBUG:root:{TICK: 221, PC: 9, HEAD: 2, TOS: 0, 0, 0}  Opcode.JNT 13 ('None' @ 15:WHILE)
DEBUG:root:{TICK: 222, PC: 13, HEAD: 1, TOS: 0, 0, 0}  Opcode.HALT  ('None' @ 14:HALT)
output: Hello world!
instr_counter:  118 ticks: 222
```

| ФИО           | алг.  | code инстр. | инстр. | такт. | вариант                                                  |
|---------------|-------|-------------|--------|-------|----------------------------------------------------------|
| Васильев М.О. | hello | 18          | 118    | 222   | forth, stack, neum, hw, tick, struct, stream, mem, prob2 |
| Васильев М.О. | cat   | 8           | 156    | 201   | forth, stack, neum, hw, tick, struct, stream, mem, prob2 |
| Васильев М.О. | prob2 | 39          | 654    | 822   | forth, stack, neum, hw, tick, struct, stream, mem, prob2 |
